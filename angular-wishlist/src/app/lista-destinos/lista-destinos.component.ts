import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  destinos: DestinoViaje[];
  updates: string[];
  constructor(public destinosApiClient:DestinosApiClient) { 
    this.onItemAdded = new EventEmitter();
	this.destinos = [];
	this.updates = [];
	this.destinosApiClient.subscribeOnChange((d: DestinoViaje)=> {
		if (d.nombre!= "") {
			this.updates.push('Se ha elegido a ' + d.nombre);
		}
	});
  }

  ngOnInit(): void {
  }
  // se puede utilizar esta funci�n llamandola 
  // desde html app-form-destino-viaje (onItemAdded)="guardar($event)"></app-form-destino-viaje>
  /*guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    return false;
  }*/
  
  // esta es la que se utiliza
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  // se puede usar esta funci�n llamandola
  // desde (onClicked)="seleccionado($event)"
  /*seleccionado(e: DestinoViaje){
    this.destinos.forEach(function (x) {x.setSelected(false); });
    e.setSelected(true);
  }*/

  // esta es la que se utiliza
  elegido(e: DestinoViaje){
    //Se substituye por el nuevo metodo del api
	//this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true);
	this.destinosApiClient.elegir(e);
	
  }

}