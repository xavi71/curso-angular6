import { createAction, props } from '@ngrx/store';
import { DestinoViaje } from '../models/destino-viaje.model'

export const getDestino = createAction('[Destino] Buscar destino');
export const addDestino = createAction(
'[Destino] A�adir destino', 
props<{destino: DestinoViaje}>()
);
export const addDestinoSuccess = createAction(
'[Destino] A�adir destino sucesivamente', 
props<{destino: DestinoViaje}>()
);
